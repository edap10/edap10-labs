#include <stdio.h>

int main()
{
  printf("*****************************************\n");
  printf("* Congratulations!                      *\n");
  printf("*                                       *\n");
  printf("* Your computer is set up to compile    *\n");
  printf("* and run C programs from the terminal. *\n");
  printf("*****************************************\n");
}
